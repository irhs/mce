#!/usr/bin/env python3

# Script to parse data from stage 1

import re
import os
import sys
import argparse
import logging
import pandas as pd

logging.basicConfig(format='[%(asctime)s %(filename)s:%(lineno)d: %(levelname)s] %(message)s')
logger = logging.getLogger('parser')

POST_IDENTIFIERS = {
              "P01": "IM_8epKDGO7ToygWBT",
              "P02": "IM_0qwlAZbuQGU3577",
              "P03": "IM_5bB4IKqtKSjQDDn",
              "P04": "IM_a4PpuCwM8XYG8w5",
              "P05": "IM_6VAfkT3GVJsfO5f",
              "P06": "IM_0wHO4HHWqltvFn7",
              "P07": "IM_4PHigiqrQQYkRBb",
              "P08": "IM_eu62u5Bc5i7E4fj",
              "P09": "IM_d0bK8EPcBY3wUh7",
              "P10": "IM_aWfmkO26WoMMfE9",
              "P11": "IM_86ABhEfr7BU4zuB",
              "P12": "IM_6nxhiGhhLftDJu5",
              "P13": "IM_6iej7FpQGaAdjHD",
              "P14": "IM_8k6uDm3vWoB4Tg9",
              "P15": "IM_7WzkV6VP7muoxMN",
              "P16": "IM_0OPNe3VwRlFOlsV",
              "P17": "IM_cRYIDF5O3ZI7BQx",
              "P18": "IM_5A9jEepurK7Yskt",
              "P19": "IM_cLRbCccTF5gC9g1",
              "P20": "IM_2rFyjUuJQECTjiB",
              "P21": "IM_5mutXEAazJTk8ER",
              "P22": "IM_8vQrjXi79ZKsf2t",
              "P23": "IM_0UjnYNOUW4PqcPb",
              "P24": "IM_0cXSsBNLUNKTzg1",
              "P25": "IM_ahMJB98RWaG17uZ",
              "P26": "IM_bjSAtULRdZuTTMN",
              "P27": "IM_0lh03GnWu3N3hHL",
              "P28": "IM_dpp0PKKKDIXxfAp",
              "P29": "IM_aX0eDsygQVqjEyN",
              "P30": "IM_9Rn1RYXHXCE5mMB",
              "C01": "IM_3Elz4vhbi6LPHE1",
              "C02": "IM_brRMHEYRhX2CB0h",
              "C03": "IM_bK1ExCIGzLKR2sd",
              "C04": "IM_9HvbEm7ws1VQXC5",
              "C05": "IM_eUKSLjWGfLyQ2eF",
              "C06": "IM_3ODpfO7Gwqo0oGF",
              "C07": "IM_09bQC07qi9vaoDz",
              "C08": "IM_9M6MUn8hXXADTWB",
              "C09": "IM_9YTVUEVbxb75Wqp",
              "C10": "IM_cZSmXjtmbnIQv1X",
            }

ACQ_identifier = "IM_1HuA7NyMTYk50zz"

def get_post_info(row, postID):
    """Returns [time, trust, share]"""
    identifier = POST_IDENTIFIERS[postID]
    time = row[f'Timing - {identifier} - Timing - Page Submit']
    if pd.isna(time):
        raise ValueError('No post found')
    trust0 = row[f'{identifier} - How much do you trust the information shared in the above post?']
    trust1 = row[f'{identifier} - How much do you trust the information shared in the above post?.1']
    trust = trust0 if pd.isna(trust1) else trust1
    share = row[f'{identifier} - Would you share this post with your friends on Facebook?']
    # if row['postOrder'] and isinstance(row['postOrder'], str):
    #     order = row['postOrder'].split(',').index(identifier)
    # else:
    #     order = 'NA'
    return [f'{time:.2f}', trust, share]

def get_ac_info(row):
    identifier = ACQ_identifier
    time, trust, share = None, None, None
    for prefix in ['', '.1', '.2', '.3']:
        time = row[f'Timing - {identifier} - Timing - Page Submit{prefix}']
        if not pd.isna(time):
            break
    else:
        raise ValueError('No post found')

    for prefix in ['', '.1', '.2', '.3', '.4', '.5', '.6']:
        trust = row[f'{identifier} - How much do you trust the information shared in the above post?{prefix}']
        if not pd.isna(trust):
            break

    for prefix in ['', '.1', '.2', '.3']:
        share = row[f'{identifier} - Would you share this post with your friends on Facebook?{prefix}']
        if not pd.isna(share):
            break

    return [f'{time:.2f}', trust, share]

def passed_attention_check(row):
    try:
        time, trust, share = get_ac_info(row)
        if pd.isna(trust) and pd.isna(share):
            return True
        else:
            return False
    except ValueError:
        return False


def ignore(row):
    ignore_mturkID = [
                        'sjhfd',
                        'shri',
                     ]
    if pd.isna(row['mturkID']):
        return True

    for pattern in ignore_mturkID:
        try:
            if row['mturkID'].find(pattern) >= 0:
                return True
        except:
            True

    return False


def parse(incsv):

    col_map = {
        "Duration (in seconds)": "duration",
        "Recorded Date": "date",
        "Timing - Page Submit": "consentPageTime",
        "Browser Meta Info - Browser": "browser",
        "Browser Meta Info - Operating System": "os",
        "Browser Meta Info - Resolution": "browserRes",
        "Enter your MTurk ID. (We use it to verify your HIT and to compensate you.)": "mturkID",
        "From where do you typically get your news? (e.g., Facebook, Twitter, Whatsapp, TV, Newspaper sites, Printed news)": "newsSource",
        "What is the highest level of formal education you have completed? - Selected Choice": "education",
        "What is your gender? - Selected Choice": "gender",
        "What is your age?": "age",
        "Is there anything else you would like to tell us? Please feel free to include any questions, concerns, or suggestions.": "comment",
        "Did you encounter any technical difficulties during this study? - Selected Choice": "technicalDifficulty",
        "completionCode": "completionCode",
        "TrustScaleLH": "trustScaleLH",
        "postOrder": "postOrder",
    }

    columns_subs = [
        'repID',
        'date',
        'duration',
        'mturkID',
        'education',
        'gender',
        'age',
        'passedAC',
    ]
    columns_posts = columns_subs + ['post', 'time', 'trust', 'willShare']

    TMP_CSV = 'tmp.csv'

    with open(TMP_CSV, 'w') as fout:
        with open(incsv, 'r') as fp:
            for num, line in enumerate(fp):
                if num in [0, 2]:
                    continue
                fout.write(line)

    outdir = os.path.dirname(incsv)
    basename = os.path.splitext(os.path.basename(incsv))[0]

    outcsv1 = f'{outdir}/{basename}-parsed-subs.csv'
    outcsv2 = f'{outdir}/{basename}-parsed-posts.csv'

    data_subs = []
    data_posts = []

    df = pd.read_csv(TMP_CSV)
    df = df.rename(columns=col_map)

    repID = 1  # response ID
    for index, row in df.iterrows():
        if ignore(row):
            continue # ignore this response

        tmp = [
                repID,
                row['date'],
                row['duration'],
                row['mturkID'],
                row['education'],
                row['gender'],
                row['age'],
                int(passed_attention_check(row)),
            ]
        repID += 1

        data_subs.append(tmp)

        for post in POST_IDENTIFIERS:
            try:
                data_posts.append(tmp + [post] + get_post_info(row, post))
            except ValueError:
                pass

    df = pd.DataFrame(data_subs, columns=columns_subs)
    df.to_csv(outcsv1, index=False)
    logger.info(f'Saved parsed data to {outcsv1}')


    df = pd.DataFrame(data_posts, columns=columns_posts)
    trust_col_map = {'Not at all': 1, 'Barely': 2, 'Somewhat': 3, 'A lot': 4, 'Entirely': 5}
    share_col_map = {'No': 0, 'Yes': 1}
    df['trust'] = df['trust'].map(trust_col_map, )
    df['willShare'] = df['willShare'].map(share_col_map)
    df.to_csv(outcsv2, index=False)
    logger.info(f'Saved parsed data to {outcsv2}')

    os.remove(TMP_CSV)




def main():
    parser = argparse.ArgumentParser(description='Aggregate survey data')
    parser.add_argument('file',
                        action='store',
                        help="CSV data file.")
    parser.add_argument('-v',
                        action='count',
                        dest='verbose',
                        default=1,
                        help="Verbose.")

    args = parser.parse_args()

    log_levels = [logging.WARNING, logging.INFO, logging.DEBUG]
    logger.setLevel(log_levels[args.verbose])

    parse(args.file)


if __name__ == '__main__':
    main()
