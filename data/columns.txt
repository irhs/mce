Start Date
End Date
Response Type
IP Address
Progress
duration
Finished
date
Response ID
Recipient Last Name
Recipient First Name
Recipient Email
External Data Reference
Location Latitude
Location Longitude
Distribution Channel
User Language
Q_RecaptchaScore
Timing - First Click
Timing - Last Click
consentPageTime
Timing - Click Count
Study: Understanding perceptions around posts shared on Facebook    Thank you for your interest in this academic research survey!    Study goal: To understand people's perceptions about the news posts shared on Facebook.    Instructions: You'll read Facebook posts and answer questions about them. Detailed instructions available on the next screen.    Estimated completion time: 10 minutes.     Privacy and Data Collection: We do not collect any personally identifiable information. Your anonymous responses will be securely stored on our server.      Contact Information: If you have questions, you may contact us at adityav@cornell.edu.
browser
Browser Meta Info - Version
os
browserRes
Timing - First Click.1
Timing - Last Click.1
Timing - Page Submit.1
Timing - Click Count.1
Timing - First Click.2
Timing - Last Click.2
Timing - Page Submit.2
Timing - Click Count.2
mturkID
We care about the quality of our data. In order for us to get the most accurate measures of your knowledge and opinions, it is important that you thoughtfully provide your best answers to each question in this study. Will you provide your best answers to each question in this study?
Timing - IM_8epKDGO7ToygWBT - Timing - First Click
Timing - IM_8epKDGO7ToygWBT - Timing - Last Click
Timing - IM_8epKDGO7ToygWBT - Timing - Page Submit
Timing - IM_8epKDGO7ToygWBT - Timing - Click Count
IM_8epKDGO7ToygWBT - How much do you trust the information shared in the above post?
IM_8epKDGO7ToygWBT - How much do you trust the information shared in the above post?.1
IM_8epKDGO7ToygWBT - Would you share this post with your friends on Facebook?
Timing - IM_0qwlAZbuQGU3577 - Timing - First Click
Timing - IM_0qwlAZbuQGU3577 - Timing - Last Click
Timing - IM_0qwlAZbuQGU3577 - Timing - Page Submit
Timing - IM_0qwlAZbuQGU3577 - Timing - Click Count
IM_0qwlAZbuQGU3577 - How much do you trust the information shared in the above post?
IM_0qwlAZbuQGU3577 - How much do you trust the information shared in the above post?.1
IM_0qwlAZbuQGU3577 - Would you share this post with your friends on Facebook?
Timing - IM_5bB4IKqtKSjQDDn - Timing - First Click
Timing - IM_5bB4IKqtKSjQDDn - Timing - Last Click
Timing - IM_5bB4IKqtKSjQDDn - Timing - Page Submit
Timing - IM_5bB4IKqtKSjQDDn - Timing - Click Count
IM_5bB4IKqtKSjQDDn - How much do you trust the information shared in the above post?
IM_5bB4IKqtKSjQDDn - How much do you trust the information shared in the above post?.1
IM_5bB4IKqtKSjQDDn - Would you share this post with your friends on Facebook?
Timing - IM_a4PpuCwM8XYG8w5 - Timing - First Click
Timing - IM_a4PpuCwM8XYG8w5 - Timing - Last Click
Timing - IM_a4PpuCwM8XYG8w5 - Timing - Page Submit
Timing - IM_a4PpuCwM8XYG8w5 - Timing - Click Count
IM_a4PpuCwM8XYG8w5 - How much do you trust the information shared in the above post?
IM_a4PpuCwM8XYG8w5 - How much do you trust the information shared in the above post?.1
IM_a4PpuCwM8XYG8w5 - Would you share this post with your friends on Facebook?
Timing - IM_6VAfkT3GVJsfO5f - Timing - First Click
Timing - IM_6VAfkT3GVJsfO5f - Timing - Last Click
Timing - IM_6VAfkT3GVJsfO5f - Timing - Page Submit
Timing - IM_6VAfkT3GVJsfO5f - Timing - Click Count
IM_6VAfkT3GVJsfO5f - How much do you trust the information shared in the above post?
IM_6VAfkT3GVJsfO5f - How much do you trust the information shared in the above post?.1
IM_6VAfkT3GVJsfO5f - Would you share this post with your friends on Facebook?
Timing - IM_0wHO4HHWqltvFn7 - Timing - First Click
Timing - IM_0wHO4HHWqltvFn7 - Timing - Last Click
Timing - IM_0wHO4HHWqltvFn7 - Timing - Page Submit
Timing - IM_0wHO4HHWqltvFn7 - Timing - Click Count
IM_0wHO4HHWqltvFn7 - How much do you trust the information shared in the above post?
IM_0wHO4HHWqltvFn7 - How much do you trust the information shared in the above post?.1
IM_0wHO4HHWqltvFn7 - Would you share this post with your friends on Facebook?
Timing - IM_4PHigiqrQQYkRBb - Timing - First Click
Timing - IM_4PHigiqrQQYkRBb - Timing - Last Click
Timing - IM_4PHigiqrQQYkRBb - Timing - Page Submit
Timing - IM_4PHigiqrQQYkRBb - Timing - Click Count
IM_4PHigiqrQQYkRBb - How much do you trust the information shared in the above post?
IM_4PHigiqrQQYkRBb - How much do you trust the information shared in the above post?.1
IM_4PHigiqrQQYkRBb - Would you share this post with your friends on Facebook?
Timing - IM_eu62u5Bc5i7E4fj - Timing - First Click
Timing - IM_eu62u5Bc5i7E4fj - Timing - Last Click
Timing - IM_eu62u5Bc5i7E4fj - Timing - Page Submit
Timing - IM_eu62u5Bc5i7E4fj - Timing - Click Count
IM_eu62u5Bc5i7E4fj - How much do you trust the information shared in the above post?
IM_eu62u5Bc5i7E4fj - How much do you trust the information shared in the above post?.1
IM_eu62u5Bc5i7E4fj - Would you share this post with your friends on Facebook?
Timing - IM_d0bK8EPcBY3wUh7 - Timing - First Click
Timing - IM_d0bK8EPcBY3wUh7 - Timing - Last Click
Timing - IM_d0bK8EPcBY3wUh7 - Timing - Page Submit
Timing - IM_d0bK8EPcBY3wUh7 - Timing - Click Count
IM_d0bK8EPcBY3wUh7 - How much do you trust the information shared in the above post?
IM_d0bK8EPcBY3wUh7 - How much do you trust the information shared in the above post?.1
IM_d0bK8EPcBY3wUh7 - Would you share this post with your friends on Facebook?
Timing - IM_aWfmkO26WoMMfE9 - Timing - First Click
Timing - IM_aWfmkO26WoMMfE9 - Timing - Last Click
Timing - IM_aWfmkO26WoMMfE9 - Timing - Page Submit
Timing - IM_aWfmkO26WoMMfE9 - Timing - Click Count
IM_aWfmkO26WoMMfE9 - How much do you trust the information shared in the above post?
IM_aWfmkO26WoMMfE9 - How much do you trust the information shared in the above post?.1
IM_aWfmkO26WoMMfE9 - Would you share this post with your friends on Facebook?
Timing - IM_1HuA7NyMTYk50zz - Timing - First Click
Timing - IM_1HuA7NyMTYk50zz - Timing - Last Click
Timing - IM_1HuA7NyMTYk50zz - Timing - Page Submit
Timing - IM_1HuA7NyMTYk50zz - Timing - Click Count
IM_1HuA7NyMTYk50zz - How much do you trust the information shared in the above post?
IM_1HuA7NyMTYk50zz - How much do you trust the information shared in the above post?.1
IM_1HuA7NyMTYk50zz - Would you share this post with your friends on Facebook?
Timing - IM_86ABhEfr7BU4zuB - Timing - First Click
Timing - IM_86ABhEfr7BU4zuB - Timing - Last Click
Timing - IM_86ABhEfr7BU4zuB - Timing - Page Submit
Timing - IM_86ABhEfr7BU4zuB - Timing - Click Count
IM_86ABhEfr7BU4zuB - How much do you trust the information shared in the above post?
IM_86ABhEfr7BU4zuB - How much do you trust the information shared in the above post?.1
IM_86ABhEfr7BU4zuB - Would you share this post with your friends on Facebook?
Timing - IM_6nxhiGhhLftDJu5 - Timing - First Click
Timing - IM_6nxhiGhhLftDJu5 - Timing - Last Click
Timing - IM_6nxhiGhhLftDJu5 - Timing - Page Submit
Timing - IM_6nxhiGhhLftDJu5 - Timing - Click Count
IM_6nxhiGhhLftDJu5 - How much do you trust the information shared in the above post?
IM_6nxhiGhhLftDJu5 - How much do you trust the information shared in the above post?.1
IM_6nxhiGhhLftDJu5 - Would you share this post with your friends on Facebook?
Timing - IM_6iej7FpQGaAdjHD - Timing - First Click
Timing - IM_6iej7FpQGaAdjHD - Timing - Last Click
Timing - IM_6iej7FpQGaAdjHD - Timing - Page Submit
Timing - IM_6iej7FpQGaAdjHD - Timing - Click Count
IM_6iej7FpQGaAdjHD - How much do you trust the information shared in the above post?
IM_6iej7FpQGaAdjHD - How much do you trust the information shared in the above post?.1
IM_6iej7FpQGaAdjHD - Would you share this post with your friends on Facebook?
Timing - IM_8k6uDm3vWoB4Tg9 - Timing - First Click
Timing - IM_8k6uDm3vWoB4Tg9 - Timing - Last Click
Timing - IM_8k6uDm3vWoB4Tg9 - Timing - Page Submit
Timing - IM_8k6uDm3vWoB4Tg9 - Timing - Click Count
IM_8k6uDm3vWoB4Tg9 - How much do you trust the information shared in the above post?
IM_8k6uDm3vWoB4Tg9 - How much do you trust the information shared in the above post?.1
IM_8k6uDm3vWoB4Tg9 - Would you share this post with your friends on Facebook?
Timing - IM_7WzkV6VP7muoxMN - Timing - First Click
Timing - IM_7WzkV6VP7muoxMN - Timing - Last Click
Timing - IM_7WzkV6VP7muoxMN - Timing - Page Submit
Timing - IM_7WzkV6VP7muoxMN - Timing - Click Count
IM_7WzkV6VP7muoxMN - How much do you trust the information shared in the above post?
IM_7WzkV6VP7muoxMN - How much do you trust the information shared in the above post?.1
IM_7WzkV6VP7muoxMN - Would you share this post with your friends on Facebook?
Timing - IM_0OPNe3VwRlFOlsV - Timing - First Click
Timing - IM_0OPNe3VwRlFOlsV - Timing - Last Click
Timing - IM_0OPNe3VwRlFOlsV - Timing - Page Submit
Timing - IM_0OPNe3VwRlFOlsV - Timing - Click Count
IM_0OPNe3VwRlFOlsV - How much do you trust the information shared in the above post?
IM_0OPNe3VwRlFOlsV - How much do you trust the information shared in the above post?.1
IM_0OPNe3VwRlFOlsV - Would you share this post with your friends on Facebook?
Timing - IM_cRYIDF5O3ZI7BQx - Timing - First Click
Timing - IM_cRYIDF5O3ZI7BQx - Timing - Last Click
Timing - IM_cRYIDF5O3ZI7BQx - Timing - Page Submit
Timing - IM_cRYIDF5O3ZI7BQx - Timing - Click Count
IM_cRYIDF5O3ZI7BQx - How much do you trust the information shared in the above post?
IM_cRYIDF5O3ZI7BQx - How much do you trust the information shared in the above post?.1
IM_cRYIDF5O3ZI7BQx - Would you share this post with your friends on Facebook?
Timing - IM_5A9jEepurK7Yskt - Timing - First Click
Timing - IM_5A9jEepurK7Yskt - Timing - Last Click
Timing - IM_5A9jEepurK7Yskt - Timing - Page Submit
Timing - IM_5A9jEepurK7Yskt - Timing - Click Count
IM_5A9jEepurK7Yskt - How much do you trust the information shared in the above post?
IM_5A9jEepurK7Yskt - How much do you trust the information shared in the above post?.1
IM_5A9jEepurK7Yskt - Would you share this post with your friends on Facebook?
Timing - IM_cLRbCccTF5gC9g1 - Timing - First Click
Timing - IM_cLRbCccTF5gC9g1 - Timing - Last Click
Timing - IM_cLRbCccTF5gC9g1 - Timing - Page Submit
Timing - IM_cLRbCccTF5gC9g1 - Timing - Click Count
IM_cLRbCccTF5gC9g1 - How much do you trust the information shared in the above post?
IM_cLRbCccTF5gC9g1 - How much do you trust the information shared in the above post?.1
IM_cLRbCccTF5gC9g1 - Would you share this post with your friends on Facebook?
Timing - IM_2rFyjUuJQECTjiB - Timing - First Click
Timing - IM_2rFyjUuJQECTjiB - Timing - Last Click
Timing - IM_2rFyjUuJQECTjiB - Timing - Page Submit
Timing - IM_2rFyjUuJQECTjiB - Timing - Click Count
IM_2rFyjUuJQECTjiB - How much do you trust the information shared in the above post?
IM_2rFyjUuJQECTjiB - How much do you trust the information shared in the above post?.1
IM_2rFyjUuJQECTjiB - Would you share this post with your friends on Facebook?
Timing - IM_1HuA7NyMTYk50zz - Timing - First Click.1
Timing - IM_1HuA7NyMTYk50zz - Timing - Last Click.1
Timing - IM_1HuA7NyMTYk50zz - Timing - Page Submit.1
Timing - IM_1HuA7NyMTYk50zz - Timing - Click Count.1
IM_1HuA7NyMTYk50zz - How much do you trust the information shared in the above post?.2
IM_1HuA7NyMTYk50zz - How much do you trust the information shared in the above post?.3
IM_1HuA7NyMTYk50zz - Would you share this post with your friends on Facebook?.1
Timing - IM_5mutXEAazJTk8ER - Timing - First Click
Timing - IM_5mutXEAazJTk8ER - Timing - Last Click
Timing - IM_5mutXEAazJTk8ER - Timing - Page Submit
Timing - IM_5mutXEAazJTk8ER - Timing - Click Count
IM_5mutXEAazJTk8ER - How much do you trust the information shared in the above post?
IM_5mutXEAazJTk8ER - How much do you trust the information shared in the above post?.1
IM_5mutXEAazJTk8ER - Would you share this post with your friends on Facebook?
Timing - IM_8vQrjXi79ZKsf2t - Timing - First Click
Timing - IM_8vQrjXi79ZKsf2t - Timing - Last Click
Timing - IM_8vQrjXi79ZKsf2t - Timing - Page Submit
Timing - IM_8vQrjXi79ZKsf2t - Timing - Click Count
IM_8vQrjXi79ZKsf2t - How much do you trust the information shared in the above post?
IM_8vQrjXi79ZKsf2t - How much do you trust the information shared in the above post?.1
IM_8vQrjXi79ZKsf2t - Would you share this post with your friends on Facebook?
Timing - IM_0UjnYNOUW4PqcPb - Timing - First Click
Timing - IM_0UjnYNOUW4PqcPb - Timing - Last Click
Timing - IM_0UjnYNOUW4PqcPb - Timing - Page Submit
Timing - IM_0UjnYNOUW4PqcPb - Timing - Click Count
IM_0UjnYNOUW4PqcPb - How much do you trust the information shared in the above post?
IM_0UjnYNOUW4PqcPb - How much do you trust the information shared in the above post?.1
IM_0UjnYNOUW4PqcPb - Would you share this post with your friends on Facebook?
Timing - IM_0cXSsBNLUNKTzg1 - Timing - First Click
Timing - IM_0cXSsBNLUNKTzg1 - Timing - Last Click
Timing - IM_0cXSsBNLUNKTzg1 - Timing - Page Submit
Timing - IM_0cXSsBNLUNKTzg1 - Timing - Click Count
IM_0cXSsBNLUNKTzg1 - How much do you trust the information shared in the above post?
IM_0cXSsBNLUNKTzg1 - How much do you trust the information shared in the above post?.1
IM_0cXSsBNLUNKTzg1 - Would you share this post with your friends on Facebook?
Timing - IM_ahMJB98RWaG17uZ - Timing - First Click
Timing - IM_ahMJB98RWaG17uZ - Timing - Last Click
Timing - IM_ahMJB98RWaG17uZ - Timing - Page Submit
Timing - IM_ahMJB98RWaG17uZ - Timing - Click Count
IM_ahMJB98RWaG17uZ - How much do you trust the information shared in the above post?
IM_ahMJB98RWaG17uZ - How much do you trust the information shared in the above post?.1
IM_ahMJB98RWaG17uZ - Would you share this post with your friends on Facebook?
Timing - IM_bjSAtULRdZuTTMN - Timing - First Click
Timing - IM_bjSAtULRdZuTTMN - Timing - Last Click
Timing - IM_bjSAtULRdZuTTMN - Timing - Page Submit
Timing - IM_bjSAtULRdZuTTMN - Timing - Click Count
IM_bjSAtULRdZuTTMN - How much do you trust the information shared in the above post?
IM_bjSAtULRdZuTTMN - How much do you trust the information shared in the above post?.1
IM_bjSAtULRdZuTTMN - Would you share this post with your friends on Facebook?
Timing - IM_0lh03GnWu3N3hHL - Timing - First Click
Timing - IM_0lh03GnWu3N3hHL - Timing - Last Click
Timing - IM_0lh03GnWu3N3hHL - Timing - Page Submit
Timing - IM_0lh03GnWu3N3hHL - Timing - Click Count
IM_0lh03GnWu3N3hHL - How much do you trust the information shared in the above post?
IM_0lh03GnWu3N3hHL - How much do you trust the information shared in the above post?.1
IM_0lh03GnWu3N3hHL - Would you share this post with your friends on Facebook?
Timing - IM_dpp0PKKKDIXxfAp - Timing - First Click
Timing - IM_dpp0PKKKDIXxfAp - Timing - Last Click
Timing - IM_dpp0PKKKDIXxfAp - Timing - Page Submit
Timing - IM_dpp0PKKKDIXxfAp - Timing - Click Count
IM_dpp0PKKKDIXxfAp - How much do you trust the information shared in the above post?
IM_dpp0PKKKDIXxfAp - How much do you trust the information shared in the above post?.1
IM_dpp0PKKKDIXxfAp - Would you share this post with your friends on Facebook?
Timing - IM_aX0eDsygQVqjEyN - Timing - First Click
Timing - IM_aX0eDsygQVqjEyN - Timing - Last Click
Timing - IM_aX0eDsygQVqjEyN - Timing - Page Submit
Timing - IM_aX0eDsygQVqjEyN - Timing - Click Count
IM_aX0eDsygQVqjEyN - How much do you trust the information shared in the above post?
IM_aX0eDsygQVqjEyN - How much do you trust the information shared in the above post?.1
IM_aX0eDsygQVqjEyN - Would you share this post with your friends on Facebook?
Timing - IM_9Rn1RYXHXCE5mMB - Timing - First Click
Timing - IM_9Rn1RYXHXCE5mMB - Timing - Last Click
Timing - IM_9Rn1RYXHXCE5mMB - Timing - Page Submit
Timing - IM_9Rn1RYXHXCE5mMB - Timing - Click Count
IM_9Rn1RYXHXCE5mMB - How much do you trust the information shared in the above post?
IM_9Rn1RYXHXCE5mMB - How much do you trust the information shared in the above post?.1
IM_9Rn1RYXHXCE5mMB - Would you share this post with your friends on Facebook?
Timing - IM_1HuA7NyMTYk50zz - Timing - First Click.2
Timing - IM_1HuA7NyMTYk50zz - Timing - Last Click.2
Timing - IM_1HuA7NyMTYk50zz - Timing - Page Submit.2
Timing - IM_1HuA7NyMTYk50zz - Timing - Click Count.2
IM_1HuA7NyMTYk50zz - How much do you trust the information shared in the above post?.4
IM_1HuA7NyMTYk50zz - How much do you trust the information shared in the above post?.5
IM_1HuA7NyMTYk50zz - Would you share this post with your friends on Facebook?.2
Timing - IM_3Elz4vhbi6LPHE1 - Timing - First Click
Timing - IM_3Elz4vhbi6LPHE1 - Timing - Last Click
Timing - IM_3Elz4vhbi6LPHE1 - Timing - Page Submit
Timing - IM_3Elz4vhbi6LPHE1 - Timing - Click Count
IM_3Elz4vhbi6LPHE1 - How much do you trust the information shared in the above post?
IM_3Elz4vhbi6LPHE1 - How much do you trust the information shared in the above post?.1
IM_3Elz4vhbi6LPHE1 - Would you share this post with your friends on Facebook?
Timing - IM_brRMHEYRhX2CB0h - Timing - First Click
Timing - IM_brRMHEYRhX2CB0h - Timing - Last Click
Timing - IM_brRMHEYRhX2CB0h - Timing - Page Submit
Timing - IM_brRMHEYRhX2CB0h - Timing - Click Count
IM_brRMHEYRhX2CB0h - How much do you trust the information shared in the above post?
IM_brRMHEYRhX2CB0h - How much do you trust the information shared in the above post?.1
IM_brRMHEYRhX2CB0h - Would you share this post with your friends on Facebook?
Timing - IM_bK1ExCIGzLKR2sd - Timing - First Click
Timing - IM_bK1ExCIGzLKR2sd - Timing - Last Click
Timing - IM_bK1ExCIGzLKR2sd - Timing - Page Submit
Timing - IM_bK1ExCIGzLKR2sd - Timing - Click Count
IM_bK1ExCIGzLKR2sd - How much do you trust the information shared in the above post?
IM_bK1ExCIGzLKR2sd - How much do you trust the information shared in the above post?.1
IM_bK1ExCIGzLKR2sd - Would you share this post with your friends on Facebook?
Timing - IM_9HvbEm7ws1VQXC5 - Timing - First Click
Timing - IM_9HvbEm7ws1VQXC5 - Timing - Last Click
Timing - IM_9HvbEm7ws1VQXC5 - Timing - Page Submit
Timing - IM_9HvbEm7ws1VQXC5 - Timing - Click Count
IM_9HvbEm7ws1VQXC5 - How much do you trust the information shared in the above post?
IM_9HvbEm7ws1VQXC5 - How much do you trust the information shared in the above post?.1
IM_9HvbEm7ws1VQXC5 - Would you share this post with your friends on Facebook?
Timing - IM_eUKSLjWGfLyQ2eF - Timing - First Click
Timing - IM_eUKSLjWGfLyQ2eF - Timing - Last Click
Timing - IM_eUKSLjWGfLyQ2eF - Timing - Page Submit
Timing - IM_eUKSLjWGfLyQ2eF - Timing - Click Count
IM_eUKSLjWGfLyQ2eF - How much do you trust the information shared in the above post?
IM_eUKSLjWGfLyQ2eF - How much do you trust the information shared in the above post?.1
IM_eUKSLjWGfLyQ2eF - Would you share this post with your friends on Facebook?
Timing - IM_3ODpfO7Gwqo0oGF - Timing - First Click
Timing - IM_3ODpfO7Gwqo0oGF - Timing - Last Click
Timing - IM_3ODpfO7Gwqo0oGF - Timing - Page Submit
Timing - IM_3ODpfO7Gwqo0oGF - Timing - Click Count
IM_3ODpfO7Gwqo0oGF - How much do you trust the information shared in the above post?
IM_3ODpfO7Gwqo0oGF - How much do you trust the information shared in the above post?.1
IM_3ODpfO7Gwqo0oGF - Would you share this post with your friends on Facebook?
Timing - IM_09bQC07qi9vaoDz - Timing - First Click
Timing - IM_09bQC07qi9vaoDz - Timing - Last Click
Timing - IM_09bQC07qi9vaoDz - Timing - Page Submit
Timing - IM_09bQC07qi9vaoDz - Timing - Click Count
IM_09bQC07qi9vaoDz - How much do you trust the information shared in the above post?
IM_09bQC07qi9vaoDz - How much do you trust the information shared in the above post?.1
IM_09bQC07qi9vaoDz - Would you share this post with your friends on Facebook?
Timing - IM_9M6MUn8hXXADTWB - Timing - First Click
Timing - IM_9M6MUn8hXXADTWB - Timing - Last Click
Timing - IM_9M6MUn8hXXADTWB - Timing - Page Submit
Timing - IM_9M6MUn8hXXADTWB - Timing - Click Count
IM_9M6MUn8hXXADTWB - How much do you trust the information shared in the above post?
IM_9M6MUn8hXXADTWB - How much do you trust the information shared in the above post?.1
IM_9M6MUn8hXXADTWB - Would you share this post with your friends on Facebook?
Timing - IM_9YTVUEVbxb75Wqp - Timing - First Click
Timing - IM_9YTVUEVbxb75Wqp - Timing - Last Click
Timing - IM_9YTVUEVbxb75Wqp - Timing - Page Submit
Timing - IM_9YTVUEVbxb75Wqp - Timing - Click Count
IM_9YTVUEVbxb75Wqp - How much do you trust the information shared in the above post?
IM_9YTVUEVbxb75Wqp - How much do you trust the information shared in the above post?.1
IM_9YTVUEVbxb75Wqp - Would you share this post with your friends on Facebook?
Timing - IM_cZSmXjtmbnIQv1X - Timing - First Click
Timing - IM_cZSmXjtmbnIQv1X - Timing - Last Click
Timing - IM_cZSmXjtmbnIQv1X - Timing - Page Submit
Timing - IM_cZSmXjtmbnIQv1X - Timing - Click Count
IM_cZSmXjtmbnIQv1X - How much do you trust the information shared in the above post?
IM_cZSmXjtmbnIQv1X - How much do you trust the information shared in the above post?.1
IM_cZSmXjtmbnIQv1X - Would you share this post with your friends on Facebook?
Timing - IM_1HuA7NyMTYk50zz - Timing - First Click.3
Timing - IM_1HuA7NyMTYk50zz - Timing - Last Click.3
Timing - IM_1HuA7NyMTYk50zz - Timing - Page Submit.3
Timing - IM_1HuA7NyMTYk50zz - Timing - Click Count.3
IM_1HuA7NyMTYk50zz - How much do you trust the information shared in the above post?.6
IM_1HuA7NyMTYk50zz - How much do you trust the information shared in the above post?.7
IM_1HuA7NyMTYk50zz - Would you share this post with your friends on Facebook?.3
Timing - First Click.3
Timing - Last Click.3
Timing - Page Submit.3
Timing - Click Count.3
newsSource
education
What is the highest level of formal education you have completed? - Other - Text
gender
What is your gender? - Prefer to self-describe: - Text
age
comment
technicalDifficulty
Did you encounter any technical difficulties during this study? - Yes (please specify) - Text
completionCode
trustScaleLH
postOrder
Q85 - Parent Topics
Q85 - Sentiment Polarity
Q85 - Sentiment Score
Q85 - Sentiment
Q85 - Topic Sentiment Label
Q85 - Topic Sentiment Score
Q85 - Topics
